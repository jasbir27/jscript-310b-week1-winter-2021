
  MATH
 

 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.

 What is the area for each of these pizzas?
 (radius would be the listed size - i.e. 13" - divided by 2)

let thirteenInchPizza=16.99
let seventeenInchPizza=19.99
let area13=Math.PI*(13/2)**2
let area17=Math.PI*(17/2)**2 


 2. What is the cost per square inch of each pizza?

 let thirteenInchPizza=16.99
let seventeenInchPizza=19.99
let area13=Math.PI*(13/2)**2
let area17=Math.PI*(17/2)**2 
let costPerSquareOf13InchPizza=16.99/area13
let costPerSquareOf17InchPizza=19.99/area17


 3. Using the Math object, put together a code snippet
 that allows you to draw a random card with a value
 between 1 and 13 (assume ace is 1, jack is 11…)

Math.round(Math.random()*13+1)


 4. Draw 3 cards and use Math to determine the highest
 card
let card1=Math.round(Math.random()*13+1)
let card2=Math.round(Math.random()*13+1)
let card3=Math.round(Math.random()*13+1)
let highestCard=Math.max(card1,card2,card3)


 ADDRESS LINE
 

 1. Create variables for firstName, lastName,
 streetAddress, city, state, and zipCode. Use
 this information to create a formatted address block
 that could be printed onto an envelope.

 let firstName="jessica"
 let lastName="paris"
 let streetAddress="12334 SE 342th pl"
 let city="kent"
 let state="Washington"
 let zipCode="98095"
 const printedAddressOnEnvelope=`${firstName} ${lastName}
 ${streetAddress}
 ${city} ${state} ${zipCode}`


 2. You are given a string in this format:
firstName lastName(assume no spaces in either)
 streetAddress
 city, state zip(could be spaces in city and state)
 
 Write code that is able to extract the first name from this string into a variable.
 Hint: use indexOf, slice, and / or substring

 let firstName="jessica"
 let lastName="paris"
 let streetAddress="12334 SE 342th pl"
 let city="kent"
 let state="Washington"
 let zipCode="98095"
 const printedAddressOnEnvelope=`${firstName} ${lastName}
 ${streetAddress}
 ${city} ${state} { } ${zipCode}`
 const indexVariable5=printedAddressOnEnvelope.slice(0,7)




 * FIND THE MIDDLE DATE

 On your own find the middle date(and time) between the following two dates:
  1/1/2020 00:00:00 and 4/1/2020 00:00:00

 Look online for documentation on Date objects.

 Starting hint:
const endDate = new Date(2019, 3, 1);

date1 = new Date(2020,01,01)
date2 = new Date(2020,04,01)
middle = new Date(date2 - (date2-date1)/2);
console.log(middle);
